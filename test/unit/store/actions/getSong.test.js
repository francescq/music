import sinon from 'sinon';
import AppleMusicAPI from '../../../../src/api/AppleMusicAPI';

import { willGetSong } from '../../../../src/store/actions';
import { WILL_SET_CURRENT_SONG } from '../../../../src/store/actions/types';

describe('getSong', () => {
    let appleStub;

    beforeEach(() => {
        appleStub = sinon.stub(AppleMusicAPI.prototype, 'getSong');
    });

    afterEach(() => {
        appleStub.restore();
    });

    it('should search the term to AppleMusicAPI', async () => {
        const willGet = willGetSong('id');

        expect(willGet).toEqual({
            type: WILL_SET_CURRENT_SONG,
            payload: 'id',
        });
    });
});
