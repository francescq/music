import { setTerm } from '../../../../src/store/actions';
import { SET_TERM } from '../../../../src/store/actions/types';

describe('setTerm', () => {
    it('should set the new term', async () => {
        const setTermAction = setTerm('lalala');

        expect(setTermAction).toEqual({ payload: 'lalala', type: SET_TERM });
    });
});
