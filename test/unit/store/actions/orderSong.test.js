import { orderSong } from '../../../../src/store/actions';
import { ORDER_SONGS } from '../../../../src/store/actions/types';

describe('orderSong', () => {
    it('should search the term to AppleMusicAPI', async () => {
        const orderAction = orderSong({ orderBy: 'a', order: 'c' });

        expect(orderAction).toEqual({
            type: ORDER_SONGS,
            payload: {
                order: 'c',
                orderBy: 'a',
            },
        });
    });
});
