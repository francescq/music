import { selectCurrentSong } from '../../../../src/store/actions';
import { SET_CURRENT_SONG } from '../../../../src/store/actions/types';

describe('selectCurrentSong', () => {
    it('should set the new term on the state', async () => {
        const currentSong = selectCurrentSong('foo');

        expect(currentSong).toEqual({
            type: SET_CURRENT_SONG,
            payload: 'foo',
        });
    });
});
