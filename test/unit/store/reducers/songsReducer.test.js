import { songsReducer } from '../../../../src/store/reducers/songsReducer';
import { GET_SONGS, ORDER_SONGS } from '../../../../src/store/actions/types';

const songs = [{ id: 0, name: 'aaa' }, { id: 1, name: 'bbb' }];

describe('songsReducer', () => {
    it('should return empty on init', () => {
        const songs = songsReducer(undefined, { type: 'foo' });

        expect(songs).toEqual([]);
    });

    describe('GET_SONGS', () => {
        it('should return the retrieved songs', () => {
            const retrievedSongs = songsReducer(undefined, {
                type: GET_SONGS,
                payload: songs,
            });

            expect(retrievedSongs).toEqual(songs);
        });
    });

    describe('ORDER_SONGS', () => {
        it('should order the current songs', () => {
            const retrievedSongs = songsReducer(songs, {
                type: ORDER_SONGS,
                payload: { orderBy: 'id' },
            });

            expect(retrievedSongs).toEqual(songs);
        });
    });
});
