import Sorter from '../../../../../src/store/reducers/utils/Sorter';

const test = [
    { name: 'bbb', number: 1 },
    { name: 'ccc', number: 11 },
    { name: 'aaa', number: 0 },
];

const sorter = new Sorter();

describe('Sorter', () => {
    it('should sort by text', () => {
        const result = sorter.sort(test, { orderBy: 'name' });

        expect(result[0].name).toEqual('aaa');
        expect(result[1].name).toEqual('bbb');
        expect(result[2].name).toEqual('ccc');
    });

    it('should sort by text desc', () => {
        const result = sorter.sort(test, { orderBy: 'name', order: 'desc' });

        expect(result[0].name).toEqual('ccc');
        expect(result[1].name).toEqual('bbb');
        expect(result[2].name).toEqual('aaa');
    });

    it('should sort by number', () => {
        const result = sorter.sort(test, { orderBy: 'number', order: 'asc' });

        expect(result[0].number).toEqual(0);
        expect(result[1].number).toEqual(1);
        expect(result[2].number).toEqual(11);
    });

    it('should sort by number desc', () => {
        const result = sorter.sort(test, { orderBy: 'number', order: 'desc' });

        expect(result[0].number).toEqual(11);
        expect(result[1].number).toEqual(1);
        expect(result[2].number).toEqual(0);
    });

    it('should fail if no orderBy is provided', () => {
        try {
            sorter.sort(test, { foo: 'id' });

            expect(true).toBe(false);
        } catch (e) {
            expect(e.message).toBe('orderby mandatory!');
        }
    });
});
