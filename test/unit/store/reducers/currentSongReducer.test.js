import { SET_CURRENT_SONG } from '../../../../src/store/actions/types';
import { currentSongReducer } from '../../../../src/store/reducers/currentSongReducer';

describe('currentSongReducer', () => {
    it('should return null on init', () => {
        const song = currentSongReducer(null, { type: 'foo', payload: 'bar' });

        expect(song).toEqual(null);
    });

    it('should set the new song', () => {
        const song = currentSongReducer(null, {
            type: SET_CURRENT_SONG,
            payload: 'mySong',
        });

        expect(song).toEqual('mySong');
    });
});
