import React from 'react';
import { shallow } from 'enzyme';

import { Header } from '../../../src/components/Header';
import Search from '../../../src/components/Search';

let props = {};
let wrapper;

describe('<Header />', () => {
    beforeEach(() => {
        props = {
            setTerm: jest.fn(),
            term: 'mySearchTerm',
            history: [],
        };
        wrapper = shallow(<Header {...props} />);
    });

    test('renders the Search', () => {
        expect(wrapper.find(Search)).toHaveLength(1);
    });

    test('render heading text', () => {
        expect(wrapper.find('#home-link').text()).toEqual('<Link />');
    });

    test('onSetTerm should call setTerm', () => {
        const myHeader = new Header(props);

        myHeader.onSetTerm('foo');

        expect(props.setTerm).toBeCalledTimes(1);
        expect(props.setTerm).toBeCalledWith('foo');
    });

    describe('<Search />', () => {
        test('should inject onSubmit on Search', () => {
            const myHeader = new Header();

            const searchProps = wrapper.find(Search).props();

            expect(searchProps.onSubmit.toString()).toEqual(
                myHeader.onSetTerm.toString()
            );
        });
    });
});
