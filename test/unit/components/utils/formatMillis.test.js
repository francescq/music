import { millisToMinutesAndSeconds } from '../../../../src/components/utils/formatMillis';

describe('formatMillis', () => {
    it('should format millis to minutes and seconds', () => {
        const result = millisToMinutesAndSeconds(10000);

        expect(result).toEqual('0:10');
    });
});
