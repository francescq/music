import React from 'react';
import { shallow } from 'enzyme';

import { SongsList } from '../../../src/components/SongsList';
import SongsListHeader from '../../../src/components/partials/SongsListHeader';

let props = {};
let songs = [];
let wrapper;

describe('<SongsList />', () => {
    describe('empty', () => {
        beforeEach(() => {
            props = {
                setTerm: jest.fn(),
                orderSong: jest.fn(),
                term: 'mySearchTerm',
                history: [],
                songs: songs,
            };

            wrapper = shallow(<SongsList {...props} />);
        });

        test('renders empty', () => {
            const empty = wrapper.find('#empty-wrapper');

            expect(empty).toHaveLength(1);
        });
    });

    describe('songs', () => {
        beforeEach(() => {
            songs = [{}, {}];
            props = {
                setTerm: jest.fn(),
                orderSong: jest.fn(),
                term: 'mySearchTerm',
                history: [],
                songs: songs,
            };

            wrapper = shallow(<SongsList {...props} />);
        });

        test('renders content-list', () => {
            const songs = wrapper.find('#content-list');

            expect(songs).toHaveLength(1);
        });

        describe('SongsListHeader', () => {
            it('should render the header with', () => {
                const header = wrapper.find(SongsListHeader);

                expect(header).toHaveLength(1);
                expect(header.props()).toEqual({
                    orderSong: props.orderSong,
                    term: 'mySearchTerm',
                });
            });
        });
    });
});
