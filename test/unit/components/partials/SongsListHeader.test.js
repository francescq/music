import React from 'react';
import { mount } from 'enzyme';

import SongsListHeader from '../../../../src/components/partials/SongsListHeader';

let wrapper;
let props;

describe('<SongsListHeader>', () => {
    beforeEach(() => {
        props = {
            term: 'myTerm',
            orderSong: jest.fn(),
        };

        wrapper = mount(<SongsListHeader {...props} />);
    });

    it('should render the current term', () => {
        const term = wrapper.find('#current-search-term');

        expect(wrapper.exists('#current-search-term')).toEqual(true);
        expect(term.text()).toEqual('myTerm');
    });

    describe('orderSongs', () => {
        it('should call order by length', () => {
            const order = wrapper.find('#order-by-length');

            order.simulate('click');

            expect(props.orderSong).toHaveBeenCalledTimes(1);
            expect(props.orderSong).toHaveBeenCalledWith({
                orderBy: 'trackTimeMillis',
            });
        });

        it('should call order by genre', () => {
            const order = wrapper.find('#order-by-genre');

            order.simulate('click');

            expect(props.orderSong).toHaveBeenCalledTimes(1);
            expect(props.orderSong).toHaveBeenCalledWith({
                orderBy: 'primaryGenreName',
            });
        });

        it('should call order by price', () => {
            const order = wrapper.find('#order-by-price');

            order.simulate('click');

            expect(props.orderSong).toHaveBeenCalledTimes(1);
            expect(props.orderSong).toHaveBeenCalledWith({
                orderBy: 'trackPrice',
            });
        });
    });
});
