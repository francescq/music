import AppleMusicAPI from '../../../src/api/AppleMusicAPI';

const getMock = {
    get: jest.fn(() => Promise.resolve({ data: {} })),
};

let appleMusicAPI;

describe('AppleMusicAPI', () => {
    beforeEach(() => {
        getMock.get.mockImplementationOnce(() => {
            Promise.resolve({
                data: { results: [] },
            });
        });

        appleMusicAPI = new AppleMusicAPI(getMock);
    });

    describe('search', () => {
        it('should get', () => {
            appleMusicAPI.search('mySearch');

            expect(getMock.get).toBeCalledWith('/search?term=mySearch');
        });
    });

    describe('getSong', () => {
        it('should getSong', () => {
            appleMusicAPI.search('mySong');

            expect(getMock.get).toBeCalledWith('/search?term=mySong');
        });
    });
});
