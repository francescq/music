# CornerJob Music Player

CornerJob Music Player simulates a music player
It uses React as main library and Redux as state management

It hits the real Itunes REST api

You can check a working version CI/CD deployed at Gitlab pages
[Music Player](https://francescq.gitlab.io/music)

# Features
- Search through Itunes 
- Get a single Song
- Plays a song
- Order fetched results
- Shares to twitter the current song

# Built demo

Music Player can be shown as dev mode and production mode

# Setup

Get it

```
git clone git@gitlab.com:francescq/music.git
cd music
```

Install

```
npm install
```

Running the tests

```
npm run test
```

Running the app

```
npm start
```

Production mode

```
npm run build
node server.js
Go to [Music Player](http://localhost:8080)
```

# Tools

-   React / Redux / Redux-Saga / React-router
-   Webpack
-   Axios
-   Jest + babel + enzyme + sinon
-   Semantic-ui: for row styling
-   Scss
-   Husky / ESLint / Prettifier

# Contribute

PRs not accepted.

# License

Do not use. This repo is going to disappear soon.
