import { SET_TERM, WILL_SET_CURRENT_SONG, RESET } from './types';
import { call, put, takeLatest } from 'redux-saga/effects';

import myAxios from '../../api/myAxios';
import AppleMusicAPI from '../../api/AppleMusicAPI';
import { getSongsAction, getSong } from '.';

const api = new AppleMusicAPI(myAxios);

export function* watchSearchSongs() {
    yield takeLatest(SET_TERM, fetchSearch);
    yield takeLatest(RESET, resetSearch);
}

function* resetSearch() {
    yield put(getSongsAction([]));
}

function* fetchSearch(action) {
    const term = action.payload;

    const fetch = term => {
        return api.search(term);
    };

    let songs = [];

    const answer = yield call(fetch, term);
    songs = answer.data.results;

    yield put(getSongsAction(songs));
}

export function* watchGetSong() {
    //console.log('sagas:watchGetSong');
    yield takeLatest(WILL_SET_CURRENT_SONG, fetchSong);
}

function* fetchSong(action) {
    //console.log('saga:watchGetSong:fetchSong', action);
    const fetch = trackId => {
        return api.getSong(trackId);
    };

    const trackId = action.payload;

    const answer = yield call(fetch, trackId);
    yield put(getSong(answer.data.results[0]));
}
