import {
    GET_SONGS,
    SET_TERM,
    SET_CURRENT_SONG,
    WILL_SET_CURRENT_SONG,
    ORDER_SONGS,
    RESET,
} from './types';
import '@babel/polyfill';

export const setTerm = term => {
    //console.log('setTerm', term);

    if (!term || term === '') {
        return { type: RESET };
    }

    return { type: SET_TERM, payload: term };
};

export function getSongsAction(songs) {
    //console.log('receiveSongs', songs);
    return {
        type: GET_SONGS,
        payload: songs,
    };
}

export const selectCurrentSong = song => {
    //console.log('selectCurrentSong', song);
    return {
        type: SET_CURRENT_SONG,
        payload: song,
    };
};

export const willGetSong = trackId => {
    //console.log('willGetSong', trackId);

    return {
        type: WILL_SET_CURRENT_SONG,
        payload: trackId,
    };
};

export const getSong = song => {
    //console.log('getSong', song);

    return {
        type: SET_CURRENT_SONG,
        payload: song,
    };
};

export const orderSong = order => {
    return {
        type: ORDER_SONGS,
        payload: order,
    };
};
