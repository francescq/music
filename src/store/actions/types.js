export const GET_SONGS = 'GET_SONGS';
export const SET_TERM = 'SET_TERM';
export const SET_CURRENT_SONG = 'SET_CURRENT_SONG';
export const SET_TERM_EMPTY = 'SET_TERM_EMPTY';
export const ORDER_SONGS = 'ORDER_SONGS';
export const WILL_SET_CURRENT_SONG = 'WILL_SET_CURRENT_SONG';
export const RESET = 'RESET';
