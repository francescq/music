import { all, call } from 'redux-saga/effects';
import { watchSearchSongs, watchGetSong } from './SongsSagas';

export default function* rootSaga() {
    yield all([call(watchSearchSongs), call(watchGetSong)]);
}
