import { combineReducers } from 'redux';

import { songsReducer } from './songsReducer';
import { termReducer } from './termReducer';
import { currentSongReducer } from './currentSongReducer';

export default combineReducers({
    songs: songsReducer,
    term: termReducer,
    currentSong: currentSongReducer,
});
