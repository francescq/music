import { SET_CURRENT_SONG } from '../actions/types';

const initialSong = null;

export const currentSongReducer = (song = initialSong, action) => {
    switch (action.type) {
        case SET_CURRENT_SONG:
            return action.payload;
        default:
            return song;
    }
};
