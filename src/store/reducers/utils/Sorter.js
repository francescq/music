class Sorter {
    compareString = (orderBy, order) => {
        return (a, b) => {
            var result =
                a[orderBy] < b[orderBy] ? -1 : a[orderBy] > b[orderBy] ? 1 : 0;
            return result * order;
        };
    };

    compareNumber = (orderBy, order) => {
        return (a, b) => {
            const compare = parseInt(a[orderBy]) - parseInt(b[orderBy]);
            return compare * order;
        };
    };

    sort = (items, { orderBy, order = 'asc' }) => {
        if (!orderBy) {
            throw Error('orderby mandatory!');
        }

        const myOrder = order === 'asc' ? 1 : -1;
        const isNumber = isNaN(items[0][orderBy]) ? false : true;
        //console.log(order, myOrder, isNumber, items[0][orderBy], orderBy);

        const comparator = isNumber ? this.compareNumber : this.compareString;

        const newItems = Object.values(items).slice();
        newItems.sort(comparator(orderBy, myOrder));

        return newItems;
    };
}

export default Sorter;
