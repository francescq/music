import { SET_TERM, RESET } from '../actions/types';

const initialTerm = '';

export const termReducer = (term = initialTerm, action) => {
    //console.log('termReducer', action);

    switch (action.type) {
        case SET_TERM:
            return action.payload;
        case RESET:
            return initialTerm;
        default:
            return term;
    }
};
