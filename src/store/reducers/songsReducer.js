import { GET_SONGS, ORDER_SONGS } from '../actions/types';
import Sorter from './utils/Sorter';

const initialSongs = [];
const sorter = new Sorter();

export const songsReducer = (songs = initialSongs, action) => {
    switch (action.type) {
        case ORDER_SONGS:
            return sorter.sort(songs, action.payload);
        case GET_SONGS:
            return [...action.payload];
        default:
            return songs;
    }
};
