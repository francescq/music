class AppleMusicAPI {
    constructor(myAxios) {
        this.myAxios = myAxios;
    }

    search(term) {
        return this.myAxios.get(`/search?term=${term}`);
    }

    getSong(trackId) {
        return this.myAxios.get(`/lookup?id=${trackId}`);
    }
}

export default AppleMusicAPI;
