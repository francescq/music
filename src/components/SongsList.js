import React from 'react';
import { connect } from 'react-redux';

import List from './utils/List';
import SongDetail from './SongDetail';
import SongsListHeader from './partials/SongsListHeader';
import { setTerm, orderSong } from '../store/actions';

import emptyIcon from '../assets/magnifying-glass_empty-state.svg';

import './SongsList.scss';

export class SongsList extends React.Component {
    componentDidMount() {
        this.props.setTerm(this.props.term);
    }

    sortSongs = orderBy => {
        this.props.orderSong({ orderBy: orderBy });
    };

    renderSong(song) {
        return <SongDetail key={song.trackId} song={song} />;
    }

    renderEmptyState() {
        return (
            <div className="content-list">
                <div id="empty-wrapper" className="content-wrapper">
                    <img
                        id="empty-icon"
                        src={emptyIcon}
                        className="empty-icon"
                    />

                    <span id="empty-message" className="empty-message">
                        Use the search bar to find songs
                    </span>
                </div>
            </div>
        );
    }

    renderSongList() {
        return (
            <div id="content-list" className="content-list">
                <SongsListHeader
                    term={this.props.term}
                    orderSong={this.props.orderSong}
                />
                <List
                    items={this.props.songs}
                    renderItem={this.renderSong}
                    className="ui grid"
                />
            </div>
        );
    }

    render() {
        //console.log(this.props.songs.length);
        if (this.props.songs.length === 0) {
            return this.renderEmptyState();
        } else {
            return this.renderSongList();
        }
    }
}

const map = (state, props) => {
    return {
        songs: Object.values(state.songs),
        term: props.match.params.term,
    };
};

export default connect(
    map,
    { setTerm, orderSong }
)(SongsList);
