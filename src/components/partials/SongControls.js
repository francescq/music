import React from 'react';
import SVG from 'react-inlinesvg';

import previousDisabled from '../../assets/player_previous_disabled.svg'; /* import nextEnabled from '../assets/player_next_enable.svg'; */
/* import previousEnabled from '../assets/player_previous_enabled.svg';
 */ import nextDisabled from '../../assets/player_next_disable.svg';
/* import previous from '../assets/player_previous.svg';
 */ import pause from '../../assets/player_pause.svg';
import play from '../../assets/player_play.svg';

import './SongControls.scss';

class SongControls extends React.Component {
    constructor(props) {
        super(props);

        this.audio = React.createRef();
        this.play = React.createRef();
    }

    playSong = () => {
        if (this.audio.current.paused) {
            //console.log('play song');
            this.audio.current.play();
            this.play.current.src = pause;
        } else {
            //console.log('pause song');
            this.audio.current.pause();
            this.play.current.src = play;
        }
    };

    render() {
        return (
            <span className="controls">
                <SVG className="icon" src={previousDisabled} alt="previous" />
                <span className="icon" onClick={this.playSong}>
                    <SVG
                        className="icon"
                        ref={this.play}
                        src={play}
                        alt="play"
                    />
                </span>
                <SVG className="icon" src={nextDisabled} alt="next" />
                <audio
                    id="audio"
                    ref={this.audio}
                    src={this.props.song.previewUrl}
                />
            </span>
        );
    }
}

export default SongControls;
