import React from 'react';
import SVG from 'react-inlinesvg';

import facebook from '../../assets/share_facebook.svg';
import twitter from '../../assets/share_twitter.svg';
import whatsapp from '../../assets/share_whatsapp.svg';

import './Social.scss';

class Social extends React.Component {
    tweetUrl = () => {
        const textToShare = 'Listen to  ' + encodeURIComponent(window.location);
        return 'https://twitter.com/intent/tweet?text=' + textToShare;
    };

    render() {
        return (
            <span className="social">
                <a
                    href={this.tweetUrl()}
                    rel="noopener noreferrer"
                    target="_blank"
                >
                    <SVG className="icon" src={twitter} alt="twitter" />
                </a>
                <SVG className="icon" src={facebook} alt="facebook" />
                <SVG className="icon" src={whatsapp} alt="whatsapp" />
            </span>
        );
    }
}

export default Social;
