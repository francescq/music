import React from 'react';

const SongsListHeader = ({ term, orderSong }) => {
    return (
        <div className="ui grid">
            <div className="row search-heading">
                Searching &quot;
                <span id="current-search-term">{term}</span>&quot;
            </div>
            <div className="one wide column" />
            <div className="three wide column">
                <span className="grey">Song</span>
            </div>
            <div className="two wide column">
                <span className="grey">Artist</span>
            </div>
            <div className="two wide column">
                <span className="grey">Album</span>
            </div>
            <div className="one wide column">
                <a
                    id="order-by-length"
                    onClick={() => orderSong({ orderBy: 'trackTimeMillis' })}
                    className="orange"
                >
                    Duration
                </a>
            </div>
            <div className="one wide column">
                <a
                    id="order-by-genre"
                    onClick={() => orderSong({ orderBy: 'primaryGenreName' })}
                    className="orange"
                >
                    Genre
                </a>
            </div>
            <div className="one wide column">
                <a
                    id="order-by-price"
                    onClick={() => orderSong({ orderBy: 'trackPrice' })}
                    className="orange"
                >
                    Price
                </a>
            </div>
        </div>
    );
};

export default SongsListHeader;
