import React from 'react';

import './ArtistDetails.scss';

const ArtistDetails = ({ song }) => {
    return (
        <div className="artist-zone">
            <img src={song.artworkUrl100} className="album-cover" />
            <span className="track-name">{song.trackName}</span>
            <span className="artist-name">{song.artistName}</span>
        </div>
    );
};

export default ArtistDetails;
