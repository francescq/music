import React from 'react';
import { connect } from 'react-redux';
import { setTerm } from '../store/actions';

import './Search.scss';

export class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = { term: props.term };
    }

    onSubmit = e => {
        e.preventDefault();

        if (this.props.term !== this.state.term) {
            this.props.onSubmit(this.state.term);
        }
    };

    onChange = e => {
        this.setState({ term: e.target.value });
    };

    render() {
        return (
            <form id="search-form" onSubmit={this.onSubmit}>
                <div className="search-box">
                    <i
                        id="search-icon"
                        onClick={this.onSubmit}
                        className="search-icon"
                    />
                    <input
                        id="search"
                        onChange={this.onChange}
                        placeholder=""
                        type="text"
                        value={this.state.term}
                    />
                </div>
            </form>
        );
    }
}

const map = state => {
    return {
        term: state.term,
    };
};

export default connect(
    map,
    { setTerm }
)(Search);
