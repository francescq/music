import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Search from './Search';
import { setTerm } from '../store/actions';

import './Header.scss';

export class Header extends React.Component {
    onSetTerm = term => {
        this.props.setTerm(term);
        this.props.history.push(`/search/${term}`);
    };

    render() {
        return (
            <div className="header-container">
                <div className="header header-layout">
                    <Link id="home-link" to="/" className="clean-link">
                        Corner Job Music Player
                    </Link>
                    <span className="search-container">
                        <Search
                            onSubmit={this.onSetTerm}
                            key={this.props.term}
                        />
                    </span>
                </div>
            </div>
        );
    }
}

const map = state => {
    return {
        term: state.term,
    };
};

export default connect(
    map,
    { setTerm }
)(Header);
