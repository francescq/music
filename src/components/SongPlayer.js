import React from 'react';
import { connect } from 'react-redux';
import { willGetSong } from '../store/actions';

import ArtistDetails from './partials/ArtistDetails';
import SongControls from './partials/SongControls';
import Social from './partials/Social';

import './SongPlayer.scss';

class SongPlayer extends React.Component {
    componentDidMount() {
        if (!this.props.song) {
            this.props.willGetSong(this.props.trackId);
        }
    }

    renderLoading() {
        return (
            <div className="player-container">Loading {this.props.trackId}</div>
        );
    }

    renderPlayer() {
        return (
            <div className="player-container">
                <ArtistDetails song={this.props.song} />
                <div className="footer">
                    <SongControls song={this.props.song} />
                    <Social song={this.props.song} />
                </div>
            </div>
        );
    }

    render() {
        return this.props.song ? this.renderPlayer() : this.renderLoading();
    }
}

const map = (state, props) => {
    return {
        song: state.currentSong,
        trackId: props.match.params.trackId,
    };
};

export default connect(
    map,
    { willGetSong }
)(SongPlayer);
