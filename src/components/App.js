import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from './Header';
import SongsList from './SongsList';
import SongPlayer from './SongPlayer';

import './App.scss';

export class App extends React.Component {
    render() {
        return (
            <div className="music-player">
                <BrowserRouter basename={process.env.PUBLIC_PATH}>
                    <Route path="/" component={Header} />
                    <Route path="/" exact component={SongsList} />
                    <Route path="/search/:term" exact component={SongsList} />
                    <Route path="/play/:trackId" component={SongPlayer} />
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
