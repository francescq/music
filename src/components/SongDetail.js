import React from 'react';
import LinesEllipsis from 'react-lines-ellipsis';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { millisToMinutesAndSeconds } from './utils/formatMillis';
import { selectCurrentSong } from '../store/actions';

import './SongDetail.scss';

class SongDetail extends React.Component {
    selectSong = () => {
        this.props.selectCurrentSong(this.props.song);
    };

    render() {
        const {
            trackName,
            artistName,
            artworkUrl60,
            trackPrice,
            collectionName,
            primaryGenreName,
            trackTimeMillis,
        } = this.props.song;

        return (
            <Link
                to={`/play/${this.props.song.trackId}`}
                onClick={this.selectSong}
                className="row song clean-link middle aligned layout"
            >
                <div className="one wide column">
                    <img src={artworkUrl60} className="track-artwork-60" />
                </div>
                <div className="three wide column track-name">
                    <span className="track">
                        <LinesEllipsis text={trackName} basedOn="letters" />
                    </span>
                </div>
                <div className="two wide column">
                    <LinesEllipsis text={artistName} basedOn="letters" />
                </div>
                <div className="two wide column">
                    <LinesEllipsis text={collectionName} basedOn="letters" />
                </div>
                <div className="one wide column">
                    {millisToMinutesAndSeconds(trackTimeMillis)}
                </div>
                <div className="one wide column">
                    <LinesEllipsis text={primaryGenreName} basedOn="letters" />
                </div>
                <div className="one wide column">
                    <LinesEllipsis text={trackPrice + ' €'} basedOn="letters" />
                </div>
            </Link>
        );
    }
}

const map = state => {
    return {
        state: state.songs,
    };
};

export default connect(
    map,
    { selectCurrentSong }
)(SongDetail);
